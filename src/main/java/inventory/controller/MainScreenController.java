
package inventory.controller;

import inventory.model.Part;
import inventory.model.Product;
import inventory.service.PartService;
import inventory.service.ProductService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class MainScreenController implements Initializable,Controller {
    
    // Declare fields
    private static int modifyPartIndex;
    private static int modifyProductIndex;
    private Logger logger = Logger.getLogger(MainScreenController.class.getName());


    // Declare methods
    static int getModifyPartIndex() {
        return modifyPartIndex;
    }
    
    static int getModifyProductIndex() {
        return modifyProductIndex;
    }

    private PartService partService;
    private ProductService productService;

    @FXML
    private TableView<Part> partsTableView;

    @FXML
    private TableColumn<Part, Integer> partsIdCol;

    @FXML
    private TableColumn<Part, String> partsNameCol;

    @FXML
    private TableColumn<Part, Integer> partsInventoryCol;

    @FXML
    private TableColumn<Part, Double> partsPriceCol;


    @FXML
    private TableView<Product> productsTableView;

    @FXML
    private TableColumn<Product, Integer> productsIdCol;

    @FXML
    private TableColumn<Product, String> productsNameCol;

    @FXML
    private TableColumn<Product, Integer> productsInventoryCol;

    @FXML
    private TableColumn<Product, Double> productsPriceCol;
    
    @FXML
    private TextField partsSearchTxt;
    
    @FXML
    private TextField productsSearchTxt;

    public MainScreenController(){
        //constructor
    }

    public void setService(PartService partService, ProductService productService){
        this.partService=partService;
        this.productService=productService;
        partsTableView.setItems(partService.getAllParts());
        productsTableView.setItems(productService.getAllProducts());
    }

    /**
     * Initializes the controller class and populate table views.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Populate parts table view
        partsIdCol.setCellValueFactory(new PropertyValueFactory<>("partId"));
        partsNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        partsInventoryCol.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        partsPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));

        // Populate products table view
        productsIdCol.setCellValueFactory(new PropertyValueFactory<>("productId"));
        productsNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        productsInventoryCol.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        productsPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
    }

    /**
     * Method to add to button handler to switch to scene passed as source
     * @param event
     * @param source
     * @throws IOException
     */
    private void displayScene(ActionEvent event, String source) throws IOException {
        Stage stage;
        Parent scene;
        stage = (Stage)((Button)event.getSource()).getScene().getWindow();
        FXMLLoader loader= new FXMLLoader(getClass().getResource(source));
        scene = loader.load();
        Controller ctrl=loader.getController();
        ctrl.setService(partService,productService);
        stage.setScene(new Scene(scene));
        stage.show();
    }

    /**
     * Ask user for confirmation before deleting selected part from list of parts.
     * @param event 
     */
    @FXML
    void handleDeletePart(ActionEvent event) {
        try {
            Part part = partsTableView.getSelectionModel().getSelectedItem();

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.initModality(Modality.NONE);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Confirm Part Deletion?");
            alert.setContentText("Are you sure you want to delete part " + part.getName() + " from parts?");
            Optional<ButtonType> result = alert.showAndWait();

            if (result.isPresent()) {
                if (result.get() == ButtonType.OK) {
                    logger.log(Level.INFO,"Part deleted.");
                    partService.deletePart(part);
                } else {
                    logger.log(Level.INFO,"Canceled part deletion.");
                }
            }
        } catch (Exception ex) {
            showUnselectedButtonAlert();
        }
    }

    /**
     * Ask user for confirmation before deleting selected product from list of products.
     * @param event 
     */
    @FXML
    void handleDeleteProduct(ActionEvent event) {
        try {
            Product product = productsTableView.getSelectionModel().getSelectedItem();

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.initModality(Modality.NONE);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Confirm Product Deletion?");
            alert.setContentText("Are you sure you want to delete product " + product.getName() + " from products?");
            Optional<ButtonType> result = alert.showAndWait();

            if (result.isPresent()) {
                if (result.get() == ButtonType.OK) {
                    productService.deleteProduct(product);
                    logger.log(Level.INFO,"Product " + product.getName() + " was removed.");
                } else {
                    logger.log(Level.INFO,"Product " + product.getName() + " was not removed.");
                }
            }
        } catch (Exception ex) {
            showUnselectedButtonAlert();
        }
    }

    /**
     * Switch scene to Add Part
     * @param event
     * @throws IOException
     */
    @FXML
    void handleAddPart(ActionEvent event) throws IOException {
        displayScene(event, "/fxml/AddPart.fxml");
    }

    /**
     * Switch scene to Add Product
     * @param event
     * @throws IOException
     */
    @FXML
    void handleAddProduct(ActionEvent event) throws IOException {
        displayScene(event, "/fxml/AddProduct.fxml");
    }

    /**
     * Changes scene to Modify Part screen and passes values of selected part
     * and its index
     * @param event
     * @throws IOException
     */
    @FXML
    void handleModifyPart(ActionEvent event) {
      try {
          Part modifyPart;

          modifyPart = partsTableView.getSelectionModel().getSelectedItem();
          modifyPartIndex = partService.getAllParts().indexOf(modifyPart);

          displayScene(event, "/fxml/ModifyPart.fxml");
      }
      catch (Exception ex){
          showUnselectedButtonAlert();
      }
    }

    /**
     * Switch scene to Modify Product
     * @param event
     * @throws IOException
     */
    @FXML
    void handleModifyProduct(ActionEvent event) {
        try{
            Product modifyProduct;

            modifyProduct = productsTableView.getSelectionModel().getSelectedItem();
            modifyProductIndex = productService.getAllProducts().indexOf(modifyProduct);

        displayScene(event, "/fxml/ModifyProduct.fxml");
        }
        catch (Exception ex){
            showUnselectedButtonAlert();
        }
    }

    private void showUnselectedButtonAlert(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error Adding Product!");
        alert.setHeaderText("Error!");
        alert.setContentText("Please select one item from list and then press the button!");
        alert.showAndWait();
    }

    /**
     * Ask user for confirmation before exiting
     * @param event 
     */
    @FXML
    void handleExit(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.initModality(Modality.NONE);
        alert.setTitle("Confirmation Needed");
        alert.setHeaderText("Confirm Exit");
        alert.setContentText("Are you sure you want to exit?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == ButtonType.OK) {
                logger.log(Level.INFO,"Ok selected. Program exited");
                System.exit(0);
            } else {
                logger.log(Level.INFO,"Cancel clicked.");
            }
        }
    }

    /**
     * Gets search text and inputs into lookupPart method to highlight desired part
     * @param event 
     */
    @FXML
    void handlePartsSearchBtn(ActionEvent event) {
        String x = partsSearchTxt.getText();
        partsTableView.getSelectionModel().select(partService.lookupPart(x));
    }

    /**
     * Gets search text and inputs into lookupProduct method to highlight desired product
     * @param event 
     */
    @FXML
    void handleProductsSearchBtn(ActionEvent event) {
        try {
            String x = productsSearchTxt.getText();

            productsTableView.getSelectionModel().select(productService.lookupProduct(x));
        }
        catch (Exception ex){
            Alert alert = new Alert(AlertType.ERROR);
            alert.initModality(Modality.NONE);
            alert.setHeaderText("Error!");
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }
}
