package inventory.service;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import javafx.collections.ObservableList;

public class PartService {
    private InventoryRepository repo;

    public PartService(InventoryRepository repo){
        this.repo =repo;
    }

    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue) throws Exception {
        String errorMessage =Part.isValidPart(name, price, inStock, min, max);
        if (errorMessage.length() >0) {
            throw new Exception(errorMessage);
        }
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(inhousePart);
    }
    public void addPart(Part p) throws Exception {
        repo.addPart(p);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue) throws Exception {
        String errorMessage = Part.isValidPart(name, price, inStock, min, max);
        if (errorMessage.length() > 0) {
            throw new Exception(errorMessage);
        }
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(outsourcedPart);
    }

    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    public void updateInhousePart(int partIndex, Part inhousePart){
        repo.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, Part outsourcedPart){
        repo.updatePart(partIndex, outsourcedPart);
    }

    public void deletePart(Part part){
        repo.deletePart(part);
    }
}