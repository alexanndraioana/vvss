package inventory.service;

import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.ObservableList;

public class ProductService {
    private InventoryRepository repo;

    public ProductService(InventoryRepository repo){
        this.repo =repo;
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts) throws Exception {
        String errorMessage = Product.isValidProduct(name, price, inStock, min, max, addParts);
        if (errorMessage.length() >0) {
            throw new Exception(errorMessage);
        }
            Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
            repo.addProduct(product);
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Product lookupProduct(String search) throws Exception {
        return repo.lookupProduct(search);
    }

    public void updateProduct(int productIndex, Product product){
        repo.updateProduct(productIndex, product);
    }

    public void deleteProduct(Product product){
        repo.deleteProduct(product);
    }
}
