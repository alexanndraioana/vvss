package inventory.repository;

import inventory.model.Part;
import inventory.model.Product;

public interface Repository {
    void readParts();

    void readProducts();

    void writeAll();

    void addPart(Part part);

    void addProduct(Product product);

    Part lookupPart(String search);

    Product lookupProduct(String search) throws Exception;

    void updatePart(int partIndex, Part part);

    void updateProduct(int productIndex, Product product);

    void deletePart(Part part);

    void deleteProduct(Product product);
}
