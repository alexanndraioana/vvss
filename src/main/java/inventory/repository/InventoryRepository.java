package inventory.repository;

import inventory.Validator.PartValidator;
import inventory.Validator.Validator;
import inventory.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InventoryRepository implements Repository {

	private static String filename = "data/items.txt";
	private Inventory inventory;
	private PartValidator validator;
	private Logger logger = Logger.getLogger(InventoryRepository.class.getName());


	public InventoryRepository(Inventory inventory, PartValidator validator) {
		this.inventory = inventory;
		this.validator = validator;
		readParts();
		readProducts();
	}

	@Override
	public void readParts() {
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		File file = null;
		try {
			file = new File(classLoader.getResource(filename).getFile());
		} catch (NullPointerException e) {
			logger.log(Level.WARNING, e.getMessage());
		}
		ObservableList<Part> listP = FXCollections.observableArrayList();
		if (file != null) {
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				while ((line = br.readLine()) != null) {
					Part part = getPartFromString(line);
					if (part != null)
						listP.add(part);

				}
			} catch (IOException e) {
				logger.log(Level.WARNING, e.getMessage());
			}
			inventory.setAllParts(listP);
		}
	}

	private Part getPartFromString(String line) {
		Part item = null;
		if (line == null || line.equals("")) return null;
		StringTokenizer st = new StringTokenizer(line, ",");
		String type = st.nextToken();
		if (type.equals("I")) {
			int id = Integer.parseInt(st.nextToken());
			inventory.setAutoPartId(id);
			String name = st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			int idMachine = Integer.parseInt(st.nextToken());
			item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
		}
		if (type.equals("O")) {
			int id = Integer.parseInt(st.nextToken());
			inventory.setAutoPartId(id);
			String name = st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String company = st.nextToken();
			item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
		}
		return item;
	}

	@Override
	public void readProducts() {
		File file = null;
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		try {
			file = new File(classLoader.getResource(filename).getFile());
		} catch (NullPointerException e) {
			e.getStackTrace();
		}

		ObservableList<Product> listP = FXCollections.observableArrayList();
		if (file != null) {
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				while ((line = br.readLine()) != null) {
					Product product = getProductFromString(line);
					if (product != null)
						listP.add(product);
				}
			} catch (IOException e) {
				logger.log(Level.WARNING, e.getMessage());
			}
			inventory.setProducts(listP);
		}
	}

	private Product getProductFromString(String line) {
		Product product = null;
		if (line == null || line.equals("")) return null;
		StringTokenizer st = new StringTokenizer(line, ",");
		String type = st.nextToken();
		if (type.equals("P")) {
			int id = Integer.parseInt(st.nextToken());
			inventory.setAutoProductId(id);
			String name = st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String partIDs = st.nextToken();

			StringTokenizer ids = new StringTokenizer(partIDs, ":");
			ObservableList<Part> list = FXCollections.observableArrayList();
			while (ids.hasMoreTokens()) {
				String idP = ids.nextToken();
				Part part = inventory.lookupPart(idP);
				if (part != null)
					list.add(part);
			}
			product = new Product(id, name, price, inStock, minStock, maxStock, list);
			product.setAssociatedParts(list);
		}
		return product;
	}

	@Override
	public void writeAll() {
		File file = null;
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		try {
			file = new File(classLoader.getResource(filename).getFile());
		} catch (NullPointerException e) {
			e.getStackTrace();
		}
		ObservableList<Part> parts = inventory.getAllParts();
		ObservableList<Product> products = inventory.getProducts();
		if (file != null) {
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(file,false))) {
				for (Part p : parts) {
					logger.log(Level.SEVERE, p::toString);
					bw.write(p.toString());
					bw.newLine();
				}

				for (Product pr : products) {
					StringBuilder line = new StringBuilder();
					line.append(pr.toString()).append(",");
					ObservableList<Part> list = pr.getAssociatedParts();
					int index = 0;
					while (index < list.size() - 1) {
						line.append(list.get(index).getPartId()).append(":");
						index++;
					}
					if (index == list.size() - 1)
						line.append(list.get(index).getPartId());
					bw.write(line.toString());
					bw.newLine();
				}
				bw.flush();
			} catch (IOException e) {
				logger.log(Level.WARNING, e.getMessage());
			}
		}
	}

	@Override
	public void addPart(Part part) {
		String err = validator.isValidPart(part);
		if (err.equals("")) {
			inventory.addPart(part);
			writeAll();
		}
	}

	@Override
	public void addProduct(Product product) {
		inventory.addProduct(product);
		writeAll();
	}

	public int getAutoPartId() {
		return inventory.getAutoPartId();
	}

	public int getAutoProductId() {
		return inventory.getAutoProductId();
	}

	public ObservableList<Part> getAllParts() {
		return inventory.getAllParts();
	}

	public ObservableList<Product> getAllProducts() {
		return inventory.getProducts();
	}

	@Override
	public Part lookupPart(String search) {
		return inventory.lookupPart(search);
	}

	@Override
	public Product lookupProduct(String search) throws Exception {
		return inventory.lookupProduct(search);
	}

	@Override
	public void updatePart(int partIndex, Part part) {
		String err = validator.isValidPart(part);
		if (err.equals("")) {
			inventory.updatePart(partIndex, part);
			writeAll();
		}
	}

	@Override
	public void updateProduct(int productIndex, Product product) {
		inventory.updateProduct(productIndex, product);
		writeAll();
	}

	@Override
	public void deletePart(Part part) {
		inventory.deletePart(part);
		writeAll();
	}

	@Override
	public void deleteProduct(Product product) {
		inventory.removeProduct(product);
		writeAll();
	}

	public void clearFile() {
		File file = null;
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		try {
			file = new File(classLoader.getResource(filename).getFile());
		} catch (NullPointerException e) {
			e.getStackTrace();
		}
		if (file != null) {
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
				bw.write("");
				inventory.init();

			} catch (IOException e) {
				logger.log(Level.WARNING, e.getMessage());
			}
		}
	}


	public Inventory getInventory(){
		return inventory;
	}

	public void setInventory(Inventory inventory){
		this.inventory=inventory;
	}
}
