package inventory.Validator;

import inventory.model.Part;

public interface Validator {
    String isValidPart(Part part);
}
