package inventory.Validator;

import inventory.model.Part;

public class PartValidator implements Validator {
    /**
     * Generate an error message for invalid values in a part
     * Valid part will return an empty string
     * @param part
     * @return
     */
    @Override
    public String isValidPart(Part part) {
        String errorMessage="";
        if(part.getName().equals("")) {
            errorMessage += "A name has not been entered. ";
        }
        if(part.getPrice() <=0.0) {
            errorMessage += "The price must be greater than $0. ";
        }
        if(part.getInStock() < 1) {
            errorMessage += "Inventory level must be greater than 0. ";
        }
        if(part.getMin() > part.getMax()) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(part.getInStock() < part.getMin()) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if(part.getInStock() > part.getMax()) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        return errorMessage;
    }
}
