package inventory;

import inventory.Validator.PartValidator;
import inventory.model.Inventory;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.controller.MainScreenController;
import inventory.service.PartService;
import inventory.service.ProductService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.logging.Level;
import java.util.logging.Logger;


public class Main extends Application {
    private Logger logger = Logger.getLogger(Main.class.getName());

    @Override
    public void start(Stage stage) throws Exception {
        PartValidator partValidator=new PartValidator();
        Inventory inventory=new Inventory();
        InventoryRepository repo = new InventoryRepository(inventory,partValidator);
        PartService partService = new PartService(repo);
        ProductService productService = new ProductService(repo);
        productService.getAllProducts().forEach(x-> logger.log(Level.INFO,x.toString()));
        partService.getAllParts().forEach(x-> logger.log(Level.INFO,x.toString()));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainScreen.fxml"));
        try {
            Parent root = loader.load();
            MainScreenController ctrl = loader.getController();
            ctrl.setService(partService,productService);

            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (Exception e)
        {
            e.getStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }
    
}
