package inventory.model;

public class PartStubMock extends Part{

    @Override
    public String getName(){
        return "T";
    }

    @Override
    public double getPrice(){
       return 4.0;
    }

    @Override
    public int getPartId() {
        return 1;
    }

    @Override
    public int getInStock() {
        return 2;
    }
    @Override
    public int getMin() {
        return 1;
    }
    @Override
    public int getMax() {
        return 4;
    }

    // Setters

}