package inventory;

import inventory.Validator.PartValidator;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.PartStubMock;
import inventory.repository.InventoryRepository;
import inventory.service.PartService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrationTestMock {
    private PartService partService;
    private InventoryRepository repository;
    private Inventory inventory = new Inventory();
    @BeforeEach
    void setUp() {
        PartValidator validator=new PartValidator();
        repository = new InventoryRepository(inventory,validator);
        repository.clearFile();
        partService = new PartService(repository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testIntegrationAdd() {
       Part p = new PartStubMock();
        try {
            partService.addPart(p);
        } catch (Exception e) {
            assert false;
        }
        ObservableList<Part> obs = inventory.getAllParts();

        assertEquals(1,obs.size());
        assertEquals("T", obs.get(0).getName());
    }

    @Test
    void testIntegrationRemove() {
        Part t = new PartStubMock();
        Part t2 = new PartStubMock();
        try {
            partService.addPart(t);
            partService.addPart(t2);
        } catch (Exception e) {
            assert false;
        }

        ObservableList<Part> obs = inventory.getAllParts();
        assertEquals(2,obs.size());
        partService.deletePart(t);
        obs = inventory.getAllParts();
        assertEquals(1,obs.size());
        assertEquals("T", obs.get(0).getName());
    }

    @Test
    void testIntegrationFind() {
        Part t = new PartStubMock();
        try {
            partService.addPart(t);
        } catch (Exception e) {
            assert false;
        }
        assertEquals(t,partService.lookupPart(t.getName()));
    }
}
