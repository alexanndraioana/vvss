package inventory;

import inventory.Validator.PartValidator;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.PartStubMock;
import inventory.repository.InventoryRepository;
import inventory.service.PartService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IntegrationTest {
    private PartService partService;
    private InventoryRepository repository;
    private Inventory inventory = new Inventory();
    @BeforeEach
    void setUp() {
        PartValidator validator=new PartValidator();
        repository = new InventoryRepository(inventory,validator);
        repository.clearFile();
        partService = new PartService(repository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testIntegrationAdd() {
        try {
            Part t = new Part("T", 4);
            partService.addPart(t);
            ObservableList<Part> obs = inventory.getAllParts();
            assertEquals(1, obs.size());
            assertEquals("T", obs.get(0).getName());
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    public void testIntegrationDelete(){
        try {
            Part t = new Part("T", 2);
            Part t2 = new Part("Tt",3);
            partService.addPart(t);
            partService.addPart(t2);

            ObservableList<Part> obs  = inventory.getAllParts();
            assertEquals(2, obs.size());
            partService.deletePart(t);
            obs = inventory.getAllParts();
            assertEquals(1, obs.size());
            assertEquals("Tt", obs.get(0).getName());
        }catch (Exception e){
            assert false;
        }
    }

    @Test
    public void testIntegrationFind(){
        try {
            Part t = new Part("part", 2);
            Part t2 = new Part("Tt",3);
            partService.addPart(t);
            partService.addPart(t2);

            assertEquals(t, partService.lookupPart(t.getName()));
        }catch (Exception e){
            assert false;
        }
    }
}
