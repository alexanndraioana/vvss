package inventory.repository;

import inventory.Validator.PartValidator;
import inventory.model.Inventory;
import inventory.model.Part;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class PartRepoTestWithMock {

    @Mock
    private Inventory inventory;

    @Mock
    private PartValidator validator;

    @InjectMocks
    private InventoryRepository repository;


    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllPartsTestSuccessfully() {
        Part part = new Part(1, "name", 2.2, 3, 1, 7);
        ObservableList<Part> parts = FXCollections.observableArrayList();
        parts.add(part);
        Mockito.when(inventory.getAllParts()).thenReturn(parts);
        assertEquals(repository.getAllParts().get(0), parts.get(0));
        Mockito.verify(inventory, Mockito.times(1)).getAllParts();
    }

    @Test
    public void addPartTestWithMockUnsuccessfully() {
        ObservableList<Part> parts = Mockito.spy(FXCollections.observableArrayList());
        Part p=new Part(1,"name", -2, 3, 1, 7);
        Mockito.when(validator.isValidPart(p)).thenReturn("The price must be greater than $0.");
        repository.addPart(p);
        Mockito.when(inventory.getAllParts()).thenReturn(parts);
        assertEquals(repository.getAllParts(),parts);
        Mockito.when(inventory.getAllParts().size()).thenReturn(0);
        assertEquals(repository.getAllParts().size(),0);
        Mockito.verify(inventory, Mockito.never()).addPart(p);
    }

    @Test
    public void addPartTestWithSpySuccessfully(){
        Part p=new Part(1,"name", 2, 3, 1, 7);
        Mockito.when(validator.isValidPart(p)).thenReturn("");
        InventoryRepository spyRepository = Mockito.spy(repository);
        Mockito.doNothing().when(spyRepository).writeAll();
        spyRepository.addPart(p);
        ObservableList<Part> parts = Mockito.spy(FXCollections.observableArrayList());
        parts.add(p);
        Mockito.when(inventory.getAllParts()).thenReturn(parts);
        assertEquals(spyRepository.getAllParts(),parts);
        assertEquals(spyRepository.getAllParts().size(),1);
        Mockito.verify(inventory, Mockito.times(1)).addPart(p);
    }

    @Test
    public void updatePartTestWithMockUnsuccessfully() {
        Part p=new Part(1,"name2", -2, 3, 1, 7);
        Mockito.when(validator.isValidPart(p)).thenReturn("The price must be greater than $0.");
        repository.updatePart(1,p);
        Mockito.verify(inventory, Mockito.never()).updatePart(1,p);
    }

    @Test
    public void updatePartTestWithSpySuccessfully() {
        Part p2=new Part(1,"name", 2, 3, 1, 7);
        Mockito.when(validator.isValidPart(p2)).thenReturn("");
        InventoryRepository spyRepository = Mockito.spy(repository);
        Mockito.doNothing().when(spyRepository).writeAll();
        spyRepository.addPart(p2);
        ObservableList<Part> parts2 = FXCollections.observableArrayList();
        parts2.add(p2);
        Mockito.when(inventory.getAllParts()).thenReturn(parts2);
        Mockito.verify(inventory, Mockito.times(1)).addPart(p2);
        assertEquals(spyRepository.getAllParts().size(), 1);
        Part p=new Part(1,"name2", 2, 3, 1, 7);
        Mockito.when(validator.isValidPart(p)).thenReturn("");
        Mockito.doNothing().when(spyRepository).writeAll();
        spyRepository.updatePart(1,p);
        ObservableList<Part> parts = FXCollections.observableArrayList();
        parts.add(p);
        Mockito.when(inventory.getAllParts()).thenReturn(parts);
        Mockito.verify(inventory, Mockito.times(1)).updatePart(1,p);
        assertEquals(spyRepository.getAllParts().size(), 1);
    }

}

