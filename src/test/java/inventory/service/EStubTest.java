package inventory.service;

import inventory.Validator.PartValidator;
import inventory.model.Inventory;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class EStubTest {

    private PartService partService;
    private InventoryRepository repository;
    @BeforeEach
    void setUp() {
        PartValidator validator=new PartValidator();
        Inventory inventory=new Inventory();
        repository = new InventoryRepository(inventory,validator);
        repository.clearFile();
        partService = new PartService(repository);

        try {
            partService.addInhousePart("name3", 2, 3, 2, 10, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addPartFail() {
        //System.out.println(partService.getAllParts().size());
        assertEquals(1,partService.getAllParts().size());
        assert 1== partService.getAllParts().size();
        assertThrows(Exception.class, () -> partService.addInhousePart("name3", 0.0, 3, 2, 10, 2));

        assert true;
        assertEquals(1,partService.getAllParts().size());
        assert 1== partService.getAllParts().size();
    }

    @Test
    void addPartSucces() {
        //System.out.println(partService.getAllParts().size());
        assertEquals(1,partService.getAllParts().size());
        assert 1== partService.getAllParts().size();
        assertDoesNotThrow(() -> partService.addInhousePart("name2", 4, 3, 2, 10, 2));

        assert true;
        assertEquals(2,partService.getAllParts().size());
        assert 2== partService.getAllParts().size();
    }
    @Test()
    void addPartThrowException() {
        assertEquals(1, partService.getAllParts().size());
        assert 1 == partService.getAllParts().size();
        try {
            partService.addInhousePart("name1", 0.0, 3, 2, 10, 2);
            Assertions.fail("Exception not thrown");
        } catch (Exception ex) {
            assert true;
        }
    }
}