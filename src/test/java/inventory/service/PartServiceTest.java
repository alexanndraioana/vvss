package inventory.service;

import inventory.Validator.PartValidator;
import inventory.Validator.Validator;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PartServiceTest {

    private PartService partService;

    @BeforeEach
    void setUp() {
        PartValidator validator=new PartValidator();
        Inventory inventory=new Inventory();
        InventoryRepository repository = new InventoryRepository(inventory,validator);
        partService = new PartService(repository);
    }

    @AfterEach
    void tearDown() {
    }

    @Order(1)
    @ParameterizedTest
    @ValueSource(ints = 3)
    @DisplayName("it should be valid because instock(${})>=1, >min(3), <max(7)")
    void addPartInStockValidErrorMessageIsNotEmptyECP(int min) {
        assertDoesNotThrow(() -> partService.addInhousePart("name", 1.1, 6, min, 7, 2));
    }

    @Test
    @Order(2)
    @DisplayName("it should be valid because price(4.4)>0.0")
    void addPartPriceValidErrorMessageIsEmptyECP(){
        assertDoesNotThrow(() -> partService.addInhousePart("name1", 4.4, 10, 2, 20, 2));
    }

    @Test
    @Order(3)
    @DisplayName("it should not be valid because price(-5.2) < 0.0")
    void addPartPriceValidErrorMessageIsNotEmptyECP(){
        assertThrows(Exception.class,()->partService.addInhousePart("name2", -5.2, 5, 2, 11, 2));
    }

    @Test
    @Order(4)
    @DisplayName("it should not be valid because instock(3) < min(6)")
    void addPartInStockValidErrorMessageIsNotEmptyECP(){
        assertThrows(Exception.class,()->partService.addInhousePart("name3", 5.0, 3, 6, 10, 2));
    }

    @Disabled
    @DisplayName("it should be Disabled")
    @Test
    void addPartTestIsDisabledECP(){
        assertThrows(Exception.class,()->partService.addInhousePart("name3", 5.0, 3, 6, 10, 2));
    }

    //BVA

    @DisplayName("it should be valid because inStock(1) >= min(1)")
    @ParameterizedTest
    @ValueSource(ints = 10)
    void addPartInStockValidErrorMessageIsEmptyBVA(int max) {
        assertDoesNotThrow(()->partService.addInhousePart("name3", 5.0, 1, 1, max, 2));
    }

    @Tag("development")
    @DisplayName("it should be valid because inStock(10) <= max(10)")
    @Test
    void addPartInStockValidErrorMessageIsEmptyBVA() {
        assertDoesNotThrow(()->partService.addInhousePart("name3", 5.0, 10, 1, 10, 2));
    }

    @DisplayName("it should be valid because price(0.1) >0.0")
    @Test
    void addPartPriceValidErrorMessageIsEmptyBVA() {
        assertDoesNotThrow(()->partService.addInhousePart("name3", 0.1, 3, 2, 10, 2));
    }

    @Tag("development")
    @DisplayName("it should not be valid because inStock(11) <= max(10) is false")
    @Test
    void addPartInStockMaxValidErrorMessageIsNotEmptyBVA() {
        assertThrows(Exception.class,()->partService.addInhousePart("name3", 5.0, 11, 1, 10, 2));
    }

    @DisplayName("it should not be valid because inStock(1) >= min(2) is false")
    @Test
    void addPartInStockMinValidErrorMessageIsNotEmptyBVA() {
        assertThrows(Exception.class,()->partService.addInhousePart("name3", 5.0, 1, 2, 10, 2));
    }

    @DisplayName("it should be disabled")
    @Disabled
    @Test
    void addPartTestIsDisabledBVA() {
        assertThrows(Exception.class,()->partService.addInhousePart("name3", 5.0, 3, 6, 10, 2));
    }

    @DisplayName("it should not be valid because price(0.0) >0.0 is false")
    @Test
    void addPartPriceValidErrorMessageIsNotEmptyBVA() {
        assertThrows(Exception.class,()->partService.addInhousePart("name3", 0.0, 3, 2, 10, 2));
    }
}