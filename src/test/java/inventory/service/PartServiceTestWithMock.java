package inventory.service;

import inventory.model.Part;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
@ExtendWith(MockitoExtension.class)
public class PartServiceTestWithMock {

    @Mock
    private InventoryRepository repository;

    @InjectMocks
    private PartService partService;

    @BeforeEach
    public void setUp(){
        //MockitoAnnotations.initMocks(this);
        partService = new PartService(repository);
    }

    @Test
    public void getPartsTestWithMockSuccessfully() {
        Part part = new Part(1, "name", 2.2, 3, 1, 7);
        ObservableList<Part> parts = FXCollections.observableArrayList();
        parts.add(part);
        Mockito.when(repository.getAllParts()).thenReturn(parts);
        assertEquals(partService.getAllParts().get(0), part);
        Mockito.verify(repository, Mockito.times(1)).getAllParts();
    }

    @Test
    public void getPartsTestWithMockThrowException() {
        assertThrows(Exception.class, () -> {
            partService.addInhousePart("name", -2, 3, 1, 7, 1);
        });
        Mockito.verify(repository, Mockito.never()).addPart(any(Part.class));
        Mockito.when(repository.getAllParts()).thenReturn(null);
        assertNull(repository.getAllParts());
        Mockito.verify(repository, Mockito.times(1)).getAllParts();
    }

    @Test
    public void getPartsTestWithSpySuccessfully(){
        PartService spyService = Mockito.spy(partService);
        Part part = new Part(1, "name", 2.2, 3, 1, 7);
        ObservableList<Part> parts = FXCollections.observableArrayList();
        ObservableList<Part> spyParts = Mockito.spy(parts);
        spyParts.add(part);
        Mockito.when(repository.getAllParts()).thenReturn(spyParts);
        assertEquals(spyService.getAllParts().get(0), part);
        Mockito.verify(repository, Mockito.times(1)).getAllParts();
    }

    @Test
    public void getPartsTestWithSpyThrowException() {
        PartService spyService = Mockito.spy(partService);
        assertThrows(Exception.class, () -> {
            spyService.addInhousePart("name", -2, 3, 1, 7, 1);
        });
        Mockito.verify(repository, Mockito.never()).addPart(any(Part.class));
        ObservableList<Part> parts = Mockito.spy(FXCollections.observableArrayList());
        Mockito.when(repository.getAllParts()).thenReturn(parts);
        assertEquals(spyService.getAllParts().size(), 0);
        Mockito.verify(repository, Mockito.times(1)).getAllParts();
    }

    @Test
    public void searchPartTestWithMock(){
        Part part = new Part(30, "numeMock", 2.5, 3, 1, 7);
        Mockito.when(repository.lookupPart("ock")).thenReturn(part);
        assertEquals(part, partService.lookupPart("ock"));
        Mockito.verify(repository, Mockito.times(1)).lookupPart("ock");
        Mockito.when(repository.lookupPart("ock")).thenReturn(null);
        assertNull(partService.lookupPart("ock"));
        Mockito.verify(repository, Mockito.times(2)).lookupPart("ock");
    }

}
