package inventory.service;

import inventory.Validator.PartValidator;
import inventory.Validator.Validator;
import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ProductServiceTest {
    private ProductService productService;
    private ObservableList<Part> parts;
    private String testName= "name1";
    private int testId= 1;
    @BeforeEach
    void setUp() {
        PartValidator validator=new PartValidator();
        Inventory inventory=new Inventory();
        InventoryRepository repo = new InventoryRepository(inventory,validator);
        productService = new ProductService(repo);
        parts = FXCollections.observableArrayList();
        parts.add(new InhousePart(testId, testName, 2, 1, 1, 10, 2));
        try {
            productService.addProduct(testName,4,2,1,6,parts);
            productService.addProduct(testName,5,2,1,6,parts);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown() {
    }

    @DisplayName("it should be valid because inStock(1) >= min(1)")
    @ParameterizedTest
    @ValueSource(ints = 10)
    void addProductTestValid1(int max) {//inStock > min inStock < max
        assertDoesNotThrow(()->productService.addProduct("num1", 10, 1, 1, max, parts));
    }

    @Tag("development")
    @DisplayName("it should be valid because inStock(10) <= max(10)")
    @Test
    void addProductTestValid2() {
        assertDoesNotThrow(()->productService.addProduct("num1", 10, 10, 1, 10, parts));
    }

    @Tag("development")
    @DisplayName("it should not be valid because inStock(11) <= max(10) is false")
    @Test
    void addProductTestNeValid1() {
        assertThrows(Exception.class,()->productService.addProduct("num1", 10, 11, 1, 10, parts));
    }

    @DisplayName("it should not be valid because inStock(0) >= min(1) is false")
    @Test
    void addProductTestNeValid2() {
        assertThrows(Exception.class,()->productService.addProduct("num1", 10, 0, 1, 10, parts));
    }

    @DisplayName("it should be disabled")
    @Disabled
    @Test
    void addProductTestDisabled() {
        assertThrows(Exception.class,()->productService.addProduct("num1", 10, 0, 1, 10, parts));
    }

    //WHITE testing

    @Test
    void lookupProductTestEmptyString() {
        assertThrows(Exception.class,()->productService.lookupProduct(""));
    }

    @Test
    void lookupProductTestNull() {
        assertThrows(NullPointerException.class,()->productService.lookupProduct(null));
    }
    @Test
    void lookupProductTestDontFind() {
        try {
            assertNull(productService.lookupProduct("acasda"));
        } catch (Exception e) {
            assert (false);
        }
    }
    @Test
    void lookupProductTestDontFindById() {
        try {
            assertNull(productService.lookupProduct("-1"));
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test
    void lookupProductTestFoundItByName() {
        try {
            assertEquals(productService.lookupProduct(testName).getName(),testName);
        } catch (Exception e) {
            assert (false);
        }
    }
    @Test
    void lookupProductTestFoundItById() {
        try {
            assertEquals(productService.lookupProduct(Integer.toString((testId))).getProductId(),testId);
        } catch (Exception e) {
            assert (false);
        }
    }
}